package com.example.tetalesson3

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.Crossfade
import androidx.compose.animation.core.TargetBasedAnimation
import androidx.compose.animation.core.VectorConverter
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.spring
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.material.icons.filled.Done
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.runtime.withFrameNanos
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.consumeAllChanges
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp
import com.example.tetalesson3.ui.theme.Green
import com.example.tetalesson3.ui.theme.TetaLesson3Theme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TetaLesson3Theme {
                Surface(modifier = Modifier
                    .padding(16.dp)
                    .fillMaxSize(),
                    color = MaterialTheme.colors.background) {
                    Slider()
                }
            }
        }
    }
}

private const val DONE_TO_ARROW_DURATION = 1_000

@Composable
fun Slider() {
    var rootSize by remember { mutableStateOf(IntSize.Zero) }
    var startAnimation by remember { mutableStateOf(false) }
    var thumbOffset by remember { mutableStateOf(0f) }
    var screenState by remember { mutableStateOf(ScreenState()) }
    var thumbWidth by remember { mutableStateOf(0f) }
    val textAlpha: Float by animateFloatAsState(1 - ((thumbOffset / (rootSize.width - thumbWidth))))

    Box(
        modifier = Modifier
            .wrapContentHeight()
            .fillMaxWidth()
            .clip(CircleShape)
            .background(Green)
            .onSizeChanged {
                rootSize = it
            }
    ) {
        TwoLinesOrderText(textAlpha)
        Thumb(
            offset = thumbOffset,
            parentSizeWidth = rootSize.width.toFloat(),
            screenState = screenState,
            updateOffsetCallback = { thumbOffset += it },
            onDragCallback = { isStartDrag ->
                startAnimation = if (isStartDrag) {
                    false
                } else {
                    !screenState.isSuccess
                }
            },
            updateThumbSizeWidth = { thumbWidth = it },
        ) { state -> screenState = state }

        LaunchedEffect(startAnimation) {
            if (startAnimation) {
                val anim = TargetBasedAnimation(
                    animationSpec = spring(stiffness = 100f),
                    typeConverter = Float.VectorConverter,
                    initialValue = thumbOffset,
                    targetValue = 0f
                )
                var playTime: Long
                val startTime = withFrameNanos { it }
                do {
                    playTime = withFrameNanos { it } - startTime
                    thumbOffset = anim.getValueFromNanos(playTime)
                } while (!anim.isFinishedFromNanos(playTime))
            }
        }
    }
}

@Composable
fun Thumb(
    offset: Float,
    parentSizeWidth: Float,
    screenState: ScreenState,
    onDragCallback: (Boolean) -> Unit,
    updateOffsetCallback: (Float) -> Unit,
    updateThumbSizeWidth: (Float) -> Unit,
    updateScreenState: (ScreenState) -> Unit,
) {
    var buttonSize by remember { mutableStateOf(IntSize.Zero) }
    val currentLocalDensity = LocalDensity.current
    val smallPadding = dimensionResource(R.dimen.padding_small)
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier
            .size(dimensionResource(R.dimen.thumb_size))
            .padding(smallPadding)
            .offset(
                x = ((calcThumbOffset(
                    futureOffset = offset,
                    maxRightOffset = maxRightOffset(
                        parentSizeWidth = parentSizeWidth,
                        buttonSizeWidth = buttonSize.width.toFloat(),
                        padding = with(currentLocalDensity) { smallPadding.toPx() },
                    ),
                    currentScreenState = screenState,
                    updateScreenState = updateScreenState,
                ) / currentLocalDensity.density)).dp
            )
            .clip(CircleShape)
            .background(Color.White)
            .onSizeChanged {
                buttonSize = it
                updateThumbSizeWidth(it.width.toFloat())
            }
            .pointerInput(Unit) {
                detectDragGestures(
                    onDragStart = { onDragCallback(true) },
                    onDragEnd = {
                        onDragCallback(false)
//                        updateStartAnimationCallback(!screenState.isSuccess)
                    },
                ) { change, dragAmount ->
                    change.consumeAllChanges()
                    updateOffsetCallback(dragAmount.x)
                }
            },
    ) {
        Crossfade(
            targetState = screenState,
            animationSpec = tween(
                durationMillis = if (!screenState.isSuccess) DONE_TO_ARROW_DURATION else 0)
        )
        {
            when (it.isSuccess) {
                true -> Icon(
                    imageVector = Icons.Default.Done,
                    contentDescription = stringResource(R.string.icon_done_content_description),
                    tint = Green,
                )
                false -> {
                    Icon(
                        imageVector = Icons.Default.ArrowForward,
                        contentDescription = stringResource(R.string.icon_start_content_description),
                        tint = Green,
                    )
                }
            }
        }
    }
}

@Composable
private fun TwoLinesOrderText(textAlpha: Float = 1f) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(dimensionResource(R.dimen.padding_small))
            .alpha(
                alpha = textAlpha
            ),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(text = stringResource(R.string.order_first_line_text), color = Color.White)
        Text(text = stringResource(R.string.order_second_line_text), color = Color.White)
    }
}

data class ScreenState(
    val isSuccess: Boolean = false,
)

private fun maxRightOffset(
    parentSizeWidth: Float,
    buttonSizeWidth: Float,
    padding: Float,
) = (parentSizeWidth - buttonSizeWidth - padding * 2)

fun calcThumbOffset(
    futureOffset: Float,
    maxRightOffset: Float,
    currentScreenState: ScreenState,
    updateScreenState: (ScreenState) -> Unit,
): Float =
    when {
        futureOffset <= 0 -> 0f.also {
            updateScreenState(
                currentScreenState.copy(
                    isSuccess = false,
                )
            )
        }
        futureOffset >= maxRightOffset -> maxRightOffset.also {
            updateScreenState(
                currentScreenState.copy(
                    isSuccess = true,
                )
            )
        }
        else -> futureOffset.also {
            updateScreenState(
                currentScreenState.copy(
                    isSuccess = false,
                )
            )
        }
    }

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    TetaLesson3Theme {
        Slider()
    }
}